import com.sun.deploy.util.ArrayUtil;

import javax.lang.model.type.PrimitiveType;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * Created by garthtee on 2/11/16.
 */
public class testing {
    public static void main(String [] args) {

//        int[] sortedList = generateSortedList(100);
//
//        for(int i : sortedList)
//            System.out.print(sortedList[i] + ", ");

        System.out.print("\nInverse List: ");

        int[] unSortedList = generateInverseList(100);

        System.out.print("\n" + unSortedList[0] + " & " + unSortedList[99]);

    }

//    /**
//     * Generates a sorted array list
//     * @param noOfElements the size the user wants the array list to be
//     * @return the array list full of sorted numbers
//     */
//    public static int[] generateSortedList(int noOfElements) {
//        Random randomGenerator = new Random();
//        int[] listRandNos = new int[noOfElements];
//
//        for (int i = 0; i < listRandNos.length; i++)
//            listRandNos[i] = randomGenerator.nextInt(100);
//
//
//        Arrays.sort(listRandNos);
//        return listRandNos;
//    }

    /**
     * Generates an inverse array list
     * @param noOfElements the size the user wants the array list to be
     * @return the inverse array list
     */
    public static int[] generateInverseList(int noOfElements) {
        int[] list = new int[noOfElements];

        for (int i = 1; i < list.length; i++) {
            list[i] = list.length-i;
            System.out.print(list[i]);
        }

        return list;
    }
}
