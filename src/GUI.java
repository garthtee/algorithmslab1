/**
 * Created by Garth on 04/02/2016.
 */

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.geom.Arc2D;
import java.util.Arrays;
import java.util.Random;

public class GUI extends JFrame implements ActionListener {

    private JLabel lbl1, lbl2, lbl3, txtArrayNo;
    private TextField txtBubble1, txtSelection1, txtInsertion1;
    private TextField txtBubble2, txtSelection2, txtInsertion2;
    private TextField txtBubble3, txtSelection3, txtInsertion3;
    private JPanel panel1, panel2, panel3, panel4, panel5;
    private JButton btnBubble, btnSelection, btnInsertion;
    private JComboBox<Integer> jcbSizes;
    private Integer[] arraySizes = {100, 1000, 10000, 100000, 1000000, 10000000};
    private int[] sortedList, unsortedList, randomList;
    private int selectedArraySize;
    private StopWatch stopWatch = new StopWatch();

    public GUI() {


        setTitle("Quadratic Algorithm Analysis");
        setLayout(new GridLayout(5, 0, 5, 5));

        // Panel 1 - Dropdown + label ***************
        panel1 = new JPanel();
        // Constructing components and adding to panel
        panel1.add(txtArrayNo = new JLabel("Please select the number of elements for you array:"));
        panel1.add(jcbSizes = new JComboBox<>(arraySizes));
        jcbSizes.setSelectedItem(null);
        jcbSizes.addItemListener(event -> {
            if (event.getStateChange() == ItemEvent.SELECTED) {             // Code reference
                Object item = event.getItem();                              // http://stackoverflow.com/a/14424530
                selectedArraySize = Integer.valueOf(item.toString());
                System.out.println("Selected Array Size: " +selectedArraySize);
            }
        });
        add(panel1);

        // Panel 2 - Sorted Array Analysis ***************
        panel2 = new JPanel();
        panel2.setLayout(new GridLayout(2, 3, 5, 5));
        // Constructing components and adding to panel
        // TextFields
        txtBubble1 = new TextField();
        txtSelection1 = new TextField();
        txtInsertion1 = new TextField();
        // Labels
        lbl1 = new JLabel("Bubble sort in milliseconds:");
        lbl2 = new JLabel("Selection sort in milliseconds:");
        lbl3 = new JLabel("Insertion sort in milliseconds:");
        // Adding components to panel 2
        panel2.add(lbl1);
        panel2.add(lbl2);
        panel2.add(lbl3);
        panel2.add(txtBubble1);
        panel2.add(txtSelection1);
        panel2.add(txtInsertion1);
        panel2.setBorder(new TitledBorder("Sorted Array Analysis"));
        add(panel2, BorderLayout.NORTH);

        // Panel 3 - Sorted Array Analysis ***************
        panel3 = new JPanel();
        panel3.setLayout(new GridLayout(2, 3, 5, 5));
        // Constructing components and adding to panel
        // TextFields
        txtBubble2 = new TextField();
        txtSelection2 = new TextField();
        txtInsertion2 = new TextField();
        lbl1 = new JLabel("Bubble sort in milliseconds:");
        lbl2 = new JLabel("Selection sort in milliseconds:");
        lbl3 = new JLabel("Insertion sort in milliseconds:");
        panel3.add(lbl1);
        panel3.add(lbl2);
        panel3.add(lbl3);
        panel3.add(txtBubble2);
        panel3.add(txtSelection2);
        panel3.add(txtInsertion2);
        panel3.setBorder(new TitledBorder("Unsorted Array Analysis"));
        add(panel3, BorderLayout.CENTER);

        // Panel 4 - Random array analysis
        panel4 = new JPanel();
        panel4.setLayout(new GridLayout(2, 3, 5, 5));
        // Constructing components and adding to panel
        // TextFields
        txtBubble3 = new TextField();
        txtSelection3 = new TextField();
        txtInsertion3 = new TextField();
        lbl1 = new JLabel("Bubble sort in milliseconds:");
        lbl2 = new JLabel("Selection sort in milliseconds:");
        lbl3 = new JLabel("Insertion sort in milliseconds:");
        panel4.add(lbl1);
        panel4.add(lbl2);
        panel4.add(lbl3);
        panel4.add(txtBubble3);
        panel4.add(txtSelection3);
        panel4.add(txtInsertion3);
        panel4.setBorder(new TitledBorder("Inverse Array Analysis"));
        add(panel4, BorderLayout.SOUTH);

        // Panel 5 - Buttons ***************
        // Constructing components and adding to panel
        panel5 = new JPanel();
        btnBubble = new JButton("Bubble");
        btnBubble.addActionListener(this);
        btnSelection = new JButton("Selection");
        btnSelection.addActionListener(this);
        btnInsertion = new JButton("Insertion");
        btnInsertion.addActionListener(this);
        panel5.add(btnBubble);
        panel5.add(btnSelection);
        panel5.add(btnInsertion);
        add(panel5);

    }

    // ActionListener
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Bubble":
                getBubbleSortAlgorithmEnhanced(generateSortedList(selectedArraySize));
                txtBubble1.setText(String.valueOf(stopWatch.getElapsedTime()) + " milliseconds!");
                stopWatch.reset();
                getBubbleSortAlgorithmEnhanced(generateRandomList(selectedArraySize));
                txtBubble2.setText(String.valueOf(stopWatch.getElapsedTime()) + " milliseconds!");
                stopWatch.reset();
                getBubbleSortAlgorithmEnhanced(generateInverseList(selectedArraySize));
                txtBubble3.setText(String.valueOf(stopWatch.getElapsedTime()) + " milliseconds!");
                stopWatch.reset();

                break;
            case "Selection":
                getSelectionSortAlgorithm(generateSortedList(selectedArraySize));
                txtSelection1.setText(String.valueOf(stopWatch.getElapsedTime()) + " milliseconds!");
                stopWatch.reset();
                getSelectionSortAlgorithm(generateRandomList(selectedArraySize));
                txtSelection2.setText(String.valueOf(stopWatch.getElapsedTime()) + " milliseconds!");
                stopWatch.reset();
                getSelectionSortAlgorithm(generateInverseList(selectedArraySize));
                txtSelection3.setText(String.valueOf(stopWatch.getElapsedTime()) + " milliseconds!");
                stopWatch.reset();
                break;
            case "Insertion":
                getInsertionSortAlgorithm(generateSortedList(selectedArraySize));
                txtInsertion1.setText(String.valueOf(stopWatch.getElapsedTime()) + " milliseconds!");
                stopWatch.reset();
                getInsertionSortAlgorithm(generateRandomList(selectedArraySize));
                txtInsertion2.setText(String.valueOf(stopWatch.getElapsedTime()) + " milliseconds!");
                stopWatch.reset();
                getInsertionSortAlgorithm(generateInverseList(selectedArraySize));
                txtInsertion3.setText(String.valueOf(stopWatch.getElapsedTime()) + " milliseconds!");
                stopWatch.reset();
                break;
        }
    }

    // Algorithms

    public int[] getBubbleSortAlgorithmOriginal(int[] list) {
        System.out.println("Bubble (Original)\n--------");
        stopWatch.start();
        int temp;
        for(int i = 0; i < list.length-1; i++){
            for(int j = 0; j < list.length-1; j++){
                if(list[j] > list[j+1]){
                    temp = list[j];
                    list[j] = list[j+1];
                    list[j+1] = temp;
                }
            }
        }
        stopWatch.stop();
        return list;
    }

    public int[] getBubbleSortAlgorithmEnhanced(int[] list) {
        System.out.println("Bubble (Enhanced)\n" +
                "--------");
        stopWatch.start();
        int temp;
        boolean sorted = false;
        for(int i = 0; i < list.length-1 && !sorted; i++){ // "&& !sorted" : End if the array is sorted
            sorted = true;
            for(int j = 0; j < list.length-1-i; j++)	{
                if(list[j] > list[j+1]){
                    temp = list[j];
                    list[j] = list[j+1];
                    list[j+1] = temp;
                    sorted = false; // If a swap occurs, the array wasn't sorted
                }
            }
        }
        stopWatch.stop();
        return list;
    }

    public int[] getSelectionSortAlgorithm(int[] list) {
        System.out.println("Selection\n" +
                "--------");
        stopWatch.start();
        for (int i = 0; i < list.length - 1; i++)      // start at the beginning of the whole array
        {
//            System.out.println("i is: " + i);
            int minimum = i;    // (1) default value of the 1st element index � use this to test against every other element.
            for (int j = i + 1; j < list.length; j++)      // (2) loop from the beginning of unsorted zone to the end of the array.
            {
                if (list[j] < list[minimum])    // compare current element to minimum
                    minimum = j;    // if it's lower, it becomes the new minimum
            }

            // swap the two values

            int temp = list[i];
            list[i] = list[minimum];
            list[minimum] = temp;
        }
        stopWatch.stop();
        return list;

    }

    public int[] getInsertionSortAlgorithm(int[] list) {
        System.out.println("Insertion\n" +
                "--------");
        stopWatch.start();
        for (int i = 1; i < list.length; i++) {
            int next = list[i];
            // find the insertion location while moving all larger element up
            int j = i;
            while (j > 0 && list[j - 1] > next) {
                list[j] = list[j - 1];
                j--;
            }
            // insert the element
            list[j] = next;
        }
        stopWatch.stop();
        return list;
    }

    /**
     * Generates an array list full of random numbers
     * @param noOfElements the size the user wants the array list to be
     * @return the array list full of random numbers
     */
    public static int[] generateRandomList(int noOfElements) {
        Random randomGenerator = new Random();
        int[] listRandNos = new int[noOfElements];

        for (int i = 0; i < listRandNos.length; i++)
            listRandNos[i] = randomGenerator.nextInt(100);


        return listRandNos;
    }

    /**
     * Generates a sorted array list
     * @param noOfElements the size the user wants the array list to be
     * @return the array list full of sorted numbers
     */
    public static int[] generateSortedList(int noOfElements) {
        Random randomGenerator = new Random();
        int[] listRandNos = new int[noOfElements];

        for (int i = 0; i < listRandNos.length; i++)
            listRandNos[i] = randomGenerator.nextInt(100);


        Arrays.sort(listRandNos);
        return listRandNos;
    }

    /**
     * Generates an inverse array list
     * @param noOfElements the size the user wants the array list to be
     * @return the inverse array list
     */
    public static int[] generateInverseList(int noOfElements) {
        int[] list = new int[noOfElements];

        for (int i = 0; i < list.length; i++) {
            list[i] = list.length-i;
        }

        return list;
    }

    /**
     * Loops through passed in array print
     * each element out separated by a comma
     * @param list list to be printed out
     */
    public void printList(int[] list) {

        System.out.println("\nList: ");

        for(int i : list)
            System.out.print(list[i] + ", ");

        System.out.println();
    }

    // Main Method
    public static void main(String [] args) {
        GUI gui = new GUI();
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setSize(900, 600);
        gui.setLocationRelativeTo(null);
        gui.setVisible(true);
    }

}